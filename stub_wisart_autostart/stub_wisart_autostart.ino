#include "TM1637.h"

//Define DEBUG Mode

#define DEBUG

// I2C ports definitions
#define CLK					9                  /* 4-digital display clock pin */
#define DIO					10                 /* 4-digiral display data pin */

// UID of the display
#define UID					0x23

// Protocol request message bytes definitions
#define REQ_S1_BYTE			0
#define REQ_S2_BYTE			1
#define REQ_UID_BYTE		2
#define REQ_TYPE_BYTE		3
#define REQ_D1_BYTE			4
#define REQ_D2_BYTE			5
#define REQ_D3_BYTE			6
#define REQ_D4_BYTE			7
#define REQ_CHECK_BYTE		8

// Protocol type of request message
#define REQ_TYPE_STATUS		0xA6
#define REQ_TYPE_SET		0xA5

//Protocol buttons definition
#define SW_WAS_PRESSED		0x31
#define SW_WASNT_PRESSED	0x30

// Protocol answer message bytes definitions
#define ANS_S1_BYTE			0
#define ANS_UID_BYTE		1
#define ANS_TYPE_BYTE		2
#define ANS_SW1_BYTE		3
#define ANS_SW2_BYTE		4
#define ANS_CHECK_BYTE		5

#define BUFFER_SIZE			9
#define ANSWER_SIZE			6

char inputBuffer[BUFFER_SIZE];
char answer[6] = { 0x04, UID, 0x99, 0x30, 0x30, 0x99 };

bool SW1_IsPressed;
bool SW2_IsPressed;

TM1637 tm1637(CLK, DIO);

void setup()
{
	Serial.begin(9600);
	pinMode(BLUE_LED, OUTPUT);
	pinMode(GREEN_LED, OUTPUT);
	pinMode(RED_LED, OUTPUT);
	pinMode(PUSH1, INPUT_PULLUP);
	pinMode(PUSH2, INPUT_PULLUP);
	LED_Blink(BLUE_LED, 300);
	LED_Blink(GREEN_LED, 300);
	LED_Blink(RED_LED, 300);
	DISPLAY_Init();
	DISPLAY_Start();

	SW1_IsPressed = false;
	SW2_IsPressed = false;
}

void loop()
{

	if (Serial.available() > 0)
	{
		Serial.readBytes(inputBuffer, BUFFER_SIZE);
#ifdef DEBUG
		UART_SendEcho();
#endif
		UART_ParseBuffer();
		LED_Blink(BLUE_LED, 100);
	}

	SWITCH_ParseValues();
}

void SWITCH_ParseValues()
{
	if (digitalRead(PUSH1) == LOW)
	{
		SW1_IsPressed = true;
		LED_Blink(RED_LED, 100);
	}

	if (digitalRead(PUSH2) == LOW)
		{
			SW2_IsPressed = true;
			LED_Blink(GREEN_LED, 100);
		}
}

void LED_Blink(uint32_t led, uint16_t delayms)
{
	digitalWrite(led, HIGH);
	delay(delayms);
	digitalWrite(led, LOW);
	delay(delayms);
}

void UART_SendEcho()
{
	Serial.print("I received > ");
	Serial.write((uint8_t *)inputBuffer, BUFFER_SIZE);
	Serial.println();
}

void UART_ParseBuffer()
{
#ifdef DEBUG
	UART_SendMsg("Start parsing");
#endif
	if (inputBuffer[REQ_UID_BYTE] == UID)
	{
#ifdef DEBUG
		UART_SendMsg("Number is valid");
#endif
		if (IsInputMsgCorrect())
		{
			LED_Blink(GREEN_LED,100);

			switch (inputBuffer[REQ_TYPE_BYTE])
			{
			case REQ_TYPE_STATUS:
				UART_SendAnswer(REQ_TYPE_STATUS);
				break;

			case REQ_TYPE_SET:
				DISPLAY_SetFromBuffer();
				UART_SendAnswer(REQ_TYPE_SET);
				break;

			default:
				break;
			}
		}
		else
		{
#ifdef DEBUG
			UART_SendMsg("Error! Wrong checksub");
#endif
			LED_Blink(RED_LED, 100);
		}
	}

}

void DISPLAY_SetDigits(uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4)
{
	tm1637.display(0, d1);
	tm1637.display(1, d2);
	tm1637.display(2, d3);
	tm1637.display(3, d4);
}

void DISPLAY_SetFromBuffer()
{
	DISPLAY_SetDigits(inputBuffer[REQ_D1_BYTE] & 0xF,
		inputBuffer[REQ_D2_BYTE] & 0xF,
		inputBuffer[REQ_D3_BYTE] & 0xF,
		inputBuffer[REQ_D4_BYTE] & 0xF
		);
}

void DISPLAY_Init()
{
	tm1637.init();
	tm1637.set(BRIGHT_TYPICAL);
	tm1637.clearDisplay();
}

void DISPLAY_Start()
{
	tm1637.clearDisplay();
	DISPLAY_SetDigits(0x7f, UID >> 4, UID & 0xF, 0x7f);
}

void UART_SendAnswer(uint8_t typeOfAnswer)
{
	if (SW1_IsPressed)
	{
		answer[ANS_SW1_BYTE] = SW_WAS_PRESSED;
	}
	else
	{
		answer[ANS_SW1_BYTE] = SW_WASNT_PRESSED;
	}

	if (SW2_IsPressed)
	{
		answer[ANS_SW2_BYTE] = SW_WAS_PRESSED;
	}
	else
	{
		answer[ANS_SW2_BYTE] = SW_WASNT_PRESSED;
	}

	answer[ANS_TYPE_BYTE] = typeOfAnswer;

	answer[ANS_CHECK_BYTE] = GetCheckSub(answer, ANSWER_SIZE - 1);

	Serial.write((uint8_t *)answer, ANSWER_SIZE);

	SW1_IsPressed = false;
	SW2_IsPressed = false;
}

void UART_SendMsg(char * msg)
{
	Serial.println();
	Serial.println(msg);
	Serial.println();
}

/**
 * \brief Checking what input message is correct by calculating check subtract
 *
 * \param None
 *
 * \return true, if message is correct and false - is invalid
 */
bool IsInputMsgCorrect()
{

	if (GetCheckSub(inputBuffer, BUFFER_SIZE - 1) == (uint8_t)inputBuffer[REQ_CHECK_BYTE])
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * \brief Function for calculating check subtract for input array
 *
 * \param[in] arr uint8_t array
 * \param[in] length lendth of array
 * \return Check subtact
 */
uint8_t GetCheckSub(char * arr, uint8_t length)
{
	uint8_t result = 0xFF;

	for (size_t i = 0; i < length; i++)
	{
		result -= *arr;
		arr++;
	}

	return result;
}
